package com.example.masakazu_hoya.mymemoapp;

import android.provider.BaseColumns;

public final class MemoContract {
    public MemoContract() {}

    public static abstract class Memos implements BaseColumns {
        public static final String TABLE_NAME = "memos";
        public static final String COL_TITLE = "titles";
        public static final String COL_BODY = "body";
        public static final String COL_CREATE = "created";
        public static final String COL_UPDATE = "updated";
    }
}
